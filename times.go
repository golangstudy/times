/*
@Time : 2018/6/13 下午2:23
@Author : tengjufeng
@File : times
@Software: GoLand
*/

package main

import (
	"baselib/times/utils"
	"fmt"
	"time"
)

func main() {
	dates := utils.FormatDate(time.Now(), utils.YYYY_MM_DD_HH_MM_SS)
	fmt.Println(dates)
}
